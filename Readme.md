<!-- Headers -->
# H1
## H2
### H3
#### H4
##### H5
###### H6


<!-- Text Styles -->
**This text is bold** 
 __This text is bold__

 *This text is italic*
 _This text is italic_

<!-- Strikethrough -->
~~Hello World~~

<!-- Quoting -->
> "Bad programmers worry about the code. Good programmers worry about data structures and their relationships.     
-Linus Torvalds"

<!-- Links -->

Go to [Google](https:/www.google.com)

<!-- Image -->

![linux](https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FLinux&psig=AOvVaw1SOuHaYgtNGPXChu_3yIVT&ust=1623266592510000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCOC597fhiPECFQAAAAAdAAAAABAD)

<!-- Lists -->
1. A    
    1. AA
2. B
    1. BA
3. C

<!-- Tasks -->

- [x] Task1
- [ ]  Task2


...bash
    git clone www.google.com
...